﻿using System.IO;
using System.Threading.Tasks;

namespace VideoStreamingWebApp.Services
{
    public interface IVideoStreamService
    {
        Task<Stream> GetVideo();
    }
}