﻿using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace VideoStreamingWebApp.Services
{
    public class VideoStreamService : IVideoStreamService
    {
        private readonly HttpClient _client;

        public VideoStreamService()
        {
            _client = new HttpClient();
        }

        public async Task<Stream> GetVideo()
        {
            const string urlBlob = "https://anthonygiretti.blob.core.windows.net/videos/nature1.mp4";

            return await _client.GetStreamAsync(urlBlob);
        }
    }
}
