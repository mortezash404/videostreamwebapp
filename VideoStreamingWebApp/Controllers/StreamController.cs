﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VideoStreamingWebApp.Services;

namespace VideoStreamingWebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StreamController : ControllerBase
    {
        private readonly IVideoStreamService _service;

        public StreamController(IVideoStreamService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<FileStreamResult> Get()
        {
            var stream = await _service.GetVideo();
            return new FileStreamResult(stream, "video/mp4");
        }
    }
}